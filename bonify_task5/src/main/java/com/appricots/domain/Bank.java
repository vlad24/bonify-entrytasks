package com.appricots.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Bank {

    @Id
    @Column(name = "bank_identifier")
    Long bankId;

    @Column(name = "bank_name", nullable = false)
    String bankName;


    public Bank() {
    }

    public Bank(Long bankId, String bankName) {
        this.bankId = bankId;
        this.bankName = bankName;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bank bank = (Bank) o;

        if (!bankId.equals(bank.bankId)) return false;
        return bankName.equals(bank.bankName);
    }


    @Override
    public int hashCode() {
        int result = bankId.hashCode();
        result = 31 * result + bankName.hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "Bank{" +
                "bankId=" + bankId +
                ", bankName='" + bankName + '\'' +
                '}';
    }


    public Long getBankId() {
        return bankId;
    }


    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }


    public String getBankName() {
        return bankName;
    }


    public void setBankName(String bankName) {
        this.bankName = bankName;
    }


}
