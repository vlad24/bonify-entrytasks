package com.appricots;

import com.appricots.dao.BankDAO;
import com.appricots.domain.Bank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;

@Component
public class BankAppDemo implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(BankAppDemo.class);

    @Value("${app.database.init.csv_file}")
    private String csvFileName;

    @Autowired
    BankDAO bankDAO;

    @Autowired
    ResourceLoader resourceLoader;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Your application started with option names : {}", args.getOptionNames());
        logger.info("Populating database table from file '{}'", csvFileName);
        ClassPathResource resource = new ClassPathResource(csvFileName);
        BufferedReader reader = new BufferedReader(new FileReader(resource.getFilename()));
        String csvLine = null;
        final String csvSeparator = ";";
        int linesRead = 0;
        while ((csvLine = reader.readLine()) != null){
            linesRead++;
            if (linesRead == 1){ // skipping head
                continue;
            }
            if (csvLine.contains(csvSeparator)) {
                String[] parts = csvLine.split(csvSeparator);
                bankDAO.save(new Bank(Long.parseLong(parts[1]), parts[0]));
            } else {
                logger.warn("Strange line detected {}", csvLine);
            }
        }
        logger.debug("Database has been populated");
        final long demoId = 10040000;
        logger.info("Displaying the bank with id = {}", demoId);
        Bank bank = bankDAO.findByBankId(demoId);
        logger.warn("Found bank: {}", bank.getBankName());
    }
}