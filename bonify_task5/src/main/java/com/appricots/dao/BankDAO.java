package com.appricots.dao;

import com.appricots.domain.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankDAO extends JpaRepository<Bank, Long> {
    Bank findByBankId(Long bankId);
}
