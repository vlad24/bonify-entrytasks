# Bonify entry task. 5

Given the following content of a csv file:
```{csv}
name;bank_identifier
Postbank;10010010
Eurocity;10030700
Commerzbank;10040000
Raiffeisenbank;22163114
```

1. Write a program which imports the entries of the csv file into a database of your
choice (preferably PostgreSQL)
2. Fetch the record with the bank identifier 10040000 from DB and print the name
of the related bank to system out.

## Solution details
The solution uses the PostgreSQL instance. Only existence of predefined ROLE and DATABASE is required.
The database is populated from init-db.csv file in the project's root
The solution is based on Hiberante ORM (just for quicker demo).

## How to run
Ensure you have a running PostgresSQL database named "bankDB" with user "test_user" having password "test_password"
Run
```{bash} 
mvn spring-boot:run
```
from project root to run the project.

To assert the correctness of the solution you cold run the tests:
```{bash}
mvn test
```

To ensure see just the name of the found bank run
```{bash}
mvn spring-boot:run | grep "Found bank"
```
