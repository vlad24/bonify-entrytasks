# Task 2

## Generic algorithm
### Definitions
* `T` : **input text**
* `C` : **categories** with their **keywords sets** `W`s (stored in a database in M2M relation). The amount of categories is `n`
```{javascript}
C = 
{
	C_1: W_1 = {K_1_1, K_1_2, ..., K_1_i1},
	C_2: W_2 = {K_2_1, K_2_2, ..., K_2_i2},
	...
	C_n: W_n = {K_n_1, K_n_2, ..., K_n_in},
}
```
* `D` : **all distinct keywords** among all categories. The size of the `D` is `d`
* `A` :  **vectorizer**. A function which maps a category `c` to a feature vector `V_c` of length `d`. Vectorizer may access the whole `C` in order to perform the mapping
* `F` : **keywords fetcher**. A function receiving a raw text and returning keywords present in the text
* `M`:  **classicator**. A function which receives a test feature vector and a set of train feature vectors and returns one among train vectors which it assumes to be closest to test one by some metric

### Generic schema of classification

#### Train phase
0. Obtaining a set of vectors `V_1`, ... `V_n` with vectorizer `A` on `C`

#### Test phase
2. Using `F` fetch keywords `J_1`, ... , `J_t` from `T` being only those contained in `D`
3. Using `A` and taking just obtained keywords as those of some 'fake' category compute test vector `V'`.
4. Using `M` obtain some vector `V_t` returning `C_t` as a result category of the input text

## On implementation
### DYI approach
0. The `C` and `D` themselves are constructed using simple SQL queries. Those queries are executed only once. The vectors of each category are obtained using  [TF-IDF](https://en.wikipedia.org/wiki/Tf�idf). It should be noted that TF weight will be binary (either 0 or 1). Our `A` based on TF IDF score will allow to diminish the effect of non uniqjue keywords during classifiacation. That is, if for some category its keyword `K_1` is contained in `m_1` keyword sets and another category keyword `K_2` in `m_2` sets with `m_1` < `m_2` then the keyword will be present in the train vector differently. The weight of `K_1` will be lower thean weight of `K_2` in the train vector of the category.
1. Test keywords may be obtained by simply splitting the document in raw text terms with some normalization processing. The normalization processing can be using by any NLP library such as (OpenNLP, scikit learn, StandfordNLP).
2. For the test keywords the result vector is computed using TF scoring.
3. Finally, the vector `V'` can be classified by `M` choosing such a catefory `C_t` for which scalar product of (`V_t`, `V'`) reaches its maximum.

### Elastic search approach
For finer results at 2 stage of the algoritm Elastic Search features can be exploited.
Elasitc Serach offers different analyzers for common languages which with proper mappings can analyze the documents after extracting keywords.
So the train phase is performed by 

0. Configuring document mapping to fetch keywords from input documents ("content" : "text", "category" : "keyword") 
0. Uploading train documents with subsequent indexing. Text of the uploaded documents may be just a comma separated keywords of that category
0. Run a More Like This Query (MLT Query) using test documents
