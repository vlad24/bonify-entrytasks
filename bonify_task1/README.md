# Task 1
Assume a blacklist IP monitoring system, where any user trying to access
 the system, is checked against a blacklist of IPs, and the connection 
 is dropped if the IP was found to be blacklisted.
Given a list of IPs (independent of size, can be anywhere from 1 record,
 to 50,000,000 records and more), design a database / cache structure to
  store those IPs for fast lookup, based on an incoming users IP.
Expected result: data model, format of the keys/values,
 description of the reasoning of your choices.

# Answer
## Proposed solution. Architecture
* Java-layer cache BlacklistedIpsManager built on top of Google Guava Cache
* PostgreSQL database with table storing IP addresses

## Proposed solution
PostgreSQL database has a table `ip_blacklisted(ip_value INET, blacklist_timestamp TIMESTAMP, etc)` with an index on ip_value column.

All requests are gone through guava cache of capacity `N` (set based on application memory constraints). 
Cache entries are IP addresses stored as bitsets (or strings, but not as just integers, due to IPv6).

If the IP is found among the addresses contained in the cache the request is dropped.
If the IP is not found, the database query is issued and if the address is blacklisted it is added to Guava cache and the request is dropped.
If there is no room for the new entry some "victim" entry is picked (customizable logic here, e.g. least frequent IP; random IP by default) and evicted to give room for a new one.

The `BlacklistedIpsManager` can also be extended with functionality of removing an IP from blacklist.
Data consistency issues arise but it is another story :)

## Reasoning
Database is needed to persist ip addresses not to lose them on some failure.

PostgreSQL has built-in support for network addresses and network masks, so the solution scales well if more complex filtering logic is required (e.g blocking some subnets or ranges of IPs). Granted, IP addresses could be stored as numbers (if only IPv4 addresses were supported - then even as integers) but I find this solution poorly scalable and not so clear.

Google Guava cache is a popular existing cache solution for concurrent environments.<br/>

The solution might work with database caching only, but caching at the application layer gives more flexibility and eliminates expensive database calls.
