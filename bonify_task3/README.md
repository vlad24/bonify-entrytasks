# Task 3
You have following code part below which consist of several “if else” statements. You
need to optimize the code to support different data types in the future without
changing it one more time.

```{java}
    public void processContract(String type) {
        if ("electricity".equals(type)) {
        System.out.println("Processed electricity");
    } else if ("dsl".equals(type)) {
        System.out.println("Processed dsl");
    } else if ("appartment_rent".equals(type)) {
        System.out.println("Processed appartment");
    }
}
```

Expected result: java Spring Boot application that can receive a “type”(dsl or other) as
an input and prints to console what was selected. Step by step explanation of how to
run the project.

## Solution details

### Interpretation & Assumptions
I was not very sure about the task statement because it was not so clear for me how strict the
requirement to not modify code was. Is only that code which was provided could not be modified?
Also it was not very clear how strictly the type set is fixed, and if it is not fixed how it is supposed to modify it.

Thinking about that I decided to "follow the hardest path" and assumed that:
* The type set is not fixed and can be expanded and shrunk
* No code modifications are allowed at all

because
* Controlling the type of input data to be in some predefined set and not allowing the set to change -- can be simply achieved with java enum usage
* Allowing any type of input data makes little sense -- in the case of the task just print the data to output :)  
It is common for applications to perform some operation on a limited set of data and
 
### Solution
The solution is a simple Spring Boot web application which serves HTTP requests at HTTP port 8080.
Initially, there are three kinds of allowed types to be processed: 'electricity', 'dsl', 'apartment'. They form  **allowed types set** (ATS).
Every request to process some type controls whether input type is contained in ATS.
ATS can be modified in runtime using add, remove operations.  


## How to use
Ensure a port 8080 is not occupied by any other app.
Run
```{bash} 
mvn spring-boot:run
```
from project root to run the project.

Three initial types are created:
* 'electricity', id = 0
* 'dsl', id = 1
* 'apartment', id = 2

Then use `curl` or gui tool like `Postman` to issue POST, DELETE requests, by which you will be able to process types and modify ATS.  

* Process type MY\_TYPE
```{url}
curl -X POST http://localhost:8080/type/process -d type=MY_TYPE
```
You will get a message about processing the type to the program output and log. 
If you sent a type not from ATS you will get a message informing you about that.

* Display current ATS
```{url}
curl -X GET http://localhost:8080/type/
```

* Add type NEW\_TYPE to ATS
```{url}
curl -X POST http://localhost:8080/type -d type=NEW_TYPE
```
You will get an id of its type to the program output.

* Delete type OLD\_TYPE from ATS by its id 
```{url}
curl -X DELETE http://localhost:8080/type?id=OLD_TYPE_ID
```
You will get the old type removed from ATS and boolean message indicating whether removal was successful.
