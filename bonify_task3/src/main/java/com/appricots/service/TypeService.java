package com.appricots.service;

import java.util.Set;

/**
 * Provides ability to process a type and control the allowed types set
 */
public interface TypeService {

    String processType(String type);

    Set<String> getAllTypes();

    Long addAllowedType(String type);

    boolean removeAllowedType(long id);

}
