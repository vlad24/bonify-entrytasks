package com.appricots.service;

import com.appricots.dao.TypeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class TypesServiceImpl implements TypeService {

    @Autowired
    TypeDAO typeDAO;

    @Override
    public String processType(String type) {
        if (typeDAO.getTypes().contains(type)) {
            return "Processed " + type;
        } else {
            return "No such type was found : " + type;
        }
    }


    @Override
    public Set<String> getAllTypes() {
        return typeDAO.getTypes();
    }

    @Override
    public Long addAllowedType(String type) {
        return typeDAO.addType(type);
    }


    @Override
    public boolean removeAllowedType(long id) {
        return typeDAO.removeType(id);
    }


}
