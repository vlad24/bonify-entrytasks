package com.appricots.controller;

import com.appricots.service.TypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class TypeController {

    private final static Logger logger = LoggerFactory.getLogger(TypeController.class);

    @Autowired
    TypeService typeService;


    @RequestMapping(value = "/type/process", method = RequestMethod.POST)
    public String processType(@RequestParam(name = "type") String type){
        String responseMessage = typeService.processType(type);
        logger.info(responseMessage);
        return responseMessage;
    }

    @RequestMapping(value = "/type", method = RequestMethod.GET)
    public Set<String> listTypes(){
        return typeService.getAllTypes();
    }


    @RequestMapping(value = "/type", method = RequestMethod.POST)
    public Long addType(@RequestParam(name = "type") String type){
        return typeService.addAllowedType(type);
    }


    @RequestMapping(value = "/type", method = RequestMethod.DELETE)
    public boolean removeTypeById(@RequestParam(name = "id") String id){
        return typeService.removeAllowedType(Long.parseLong(id));
    }

}
