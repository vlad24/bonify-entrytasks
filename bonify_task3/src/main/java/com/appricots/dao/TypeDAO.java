package com.appricots.dao;

import java.util.Set;

/**
 * Storage of allowed for processing types
 */
public interface TypeDAO {

    Long addType(String type);

    boolean removeType(long id);

    Set<String> getTypes();
}
