package com.appricots.dao;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Thread safe naive implementation of type dao which uses local set to store types
 */
@Repository
public class TypeDAOLocalImpl implements TypeDAO {

    //fake database storage having some default types
    private ConcurrentHashMap<Long, String> typeStorage = new ConcurrentHashMap<>();

    // guarded by this
    private long maxId = 0;

    public TypeDAOLocalImpl(){
        fillInitialData();
    }


    @Override
    public Long addType(String type) {
        Long idToReturn = null;
        if (!typeStorage.containsValue(type)){
            synchronized (this){
                if (!typeStorage.containsValue(type)){
                    idToReturn = maxId++;
                    typeStorage.put(idToReturn, type);
                }
            }
        }
        return idToReturn;
    }

    @Override
    public boolean removeType(long id) {
        typeStorage.remove(id);
        return true;
    }


    @Override
    public Set<String> getTypes() {
        // we could load those types from some kind of a database, file or just use some predefined enum
        return new HashSet<>(typeStorage.values());
    }

    private synchronized void fillInitialData() {
        List<String> initialTypes = Arrays.asList("electricity", "dsl", "apartment");
        initialTypes.forEach(this::addType);
    }

}
