package com.appricots.util;

/**
 * String utilities
 */
public class StringAnalysisUtil {

    /**
     * Indicates if the string resembles a name. The name is required to be longer than 2 charachters long and be lowercase execept for the first letter
     * @param string string to examine
     * @return true if the string resembles name
     */
    public static boolean isSimilarToName(String string){
        if (string.length() <= 2){
            return false;
        } else {
            String firstLetter = string.substring(0, 1);
            String remainder = string.substring(1);
            return firstLetter.toUpperCase().equals(firstLetter) && remainder.toLowerCase().equals(remainder);
        }
    }


    /**
     * Indicates whether the input string represents a parsable integer in 10 radix
     * @param string string to examine
     * @return true if the string represents a parsable integer in 10 radix
     */
    public static boolean isParsableInteger(String string){
        try{
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }
}
