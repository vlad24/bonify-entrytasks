package com.appricots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlAnalysisWebApp {
    public static void main( String[] args ) {
            SpringApplication.run(new Class<?>[] {UrlAnalysisWebApp.class}, args);
    }
}
