package com.appricots.domain;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * Entity representing the sequence of url parts. A facade to a simple set of url sequence elements
 */
public class UrlSequence implements Iterable<UrlSequence.UrlSequenceElement>{

    private List<UrlSequenceElement> elements;

    private UrlSequence(List<String> urlParts) {
        elements = urlParts.stream().map(UrlSequenceElement::new).collect(toList());
    }


    static UrlSequence from(String url) {
        String filteredUrl = url;
        int queryStringMarkerPosition = url.indexOf("?");
        if (queryStringMarkerPosition > 0){
            filteredUrl = url.substring(0, queryStringMarkerPosition);
        }
        return new UrlSequence(Arrays.stream(filteredUrl.split("/")).skip(1).collect(toList()));
    }

    String toUrlString() {
        return "/" + elements.stream().map(UrlSequenceElement::getMainValue).collect(Collectors.toList()).stream().collect(joining("/"));
    }

    Optional<String> getHead(){
        return elements.isEmpty() ? Optional.empty() : Optional.of(elements.get(0).getMainValue());
    }

    void addObservedElement(int position, String value){
        if (elements.size() <= position){
            elements.add(new UrlSequenceElement(value));
        } else {
            UrlSequenceElement lastElement = elements.get(position);
            lastElement.registerObservedValue(value);
        }
    }


    @Override
    public String toString() {
        return "||" + elements + "||";
    }


    @Override
    public Iterator<UrlSequenceElement> iterator() {
        return elements.iterator();
    }


    /**
     * Represents a logical part of a url and holds all the possible values this part can have
     */
    public static class UrlSequenceElement {

        private String mainValue;
        private Set<String> possibleValues;


        UrlSequenceElement(String mainValue) {
            this.mainValue = mainValue;
            this.possibleValues = new LinkedHashSet<>();
            possibleValues.add(mainValue);
        }

        void registerObservedValue(String alternativeValue){
            this.possibleValues.add(alternativeValue);
        }


        String getMainValue() {
            return mainValue;
        }


        void setMainValue(String mainValue) {
            this.mainValue = mainValue;
        }

        Set<String> getPossibleValues() {
            return possibleValues;
        }

        boolean isDynamic(){
            return possibleValues.size() > 1;
        }


        @Override
        public String toString() {
            return "<" + mainValue + " " + possibleValues + ">";
        }
    }
}
