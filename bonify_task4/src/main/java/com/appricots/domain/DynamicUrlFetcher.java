package com.appricots.domain;

import com.appricots.util.StringAnalysisUtil;

import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * Given a set of urls detects dynamic patterns among them
 */
public class DynamicUrlFetcher {

    private Set<String> rawUrls;


    public DynamicUrlFetcher() {
        rawUrls = new LinkedHashSet<>();
    }


    /**
     * Get raw urls which the fetcher is operating on
     * @return string set of urls
     */
    public Set<String> getRawUrls() {
        return rawUrls;
    }


    /**
     * Add raw url the fetcher will use for analysis
     * @param url the url to add
     */
    public void addUrl(String url){
        rawUrls.add(url);
    }


    /**
     * Clears all the urls added to the fetcher
     */
    public void clearRawUrls(){
        rawUrls.clear();
    }


    /**
     * Detects dynamic patterns in the set of raw urls (see {@link #getRawUrls()}
     * For example, given the set  {"/A/XX/B?p=0", /"A/YY/B", "A/ZZ/B"} the fetcher will recognize the pattern "/A/{A_query}/B. <br/>
     * <b>Note:</b> the method does not consider prefix relations between urls, that is e.g. {"/A/f/g", "A/f"} will not be considered as candidates for forming a dynamic pattern <br/>
     * The method makes some basic guesses when encountering variable part of a query: e.g. if all values of variable url part are numeric the method assumes the part stands for some sort of id.
     * @return list of detected dynamic patterns
     */
    public List<String> detectDynamicPatterns(){
        List<UrlSequence> urlSequences = buildUrlSequencesFromRawUrls();
        List<UrlSequence> prettifiedDynamicSequences = getPrettifiedDynamicSequences(urlSequences);
        return prettifiedDynamicSequences.stream()
                .map(UrlSequence::toUrlString)
                .collect(toList());
    }


    private List<UrlSequence> getPrettifiedDynamicSequences(List<UrlSequence> urlSequences) {
        List<UrlSequence> dynamicSequences = new ArrayList<>(urlSequences.size());
        for (UrlSequence sequence : urlSequences){
            boolean urlSequenceContainsDynamicElement = false;
            Iterator<UrlSequence.UrlSequenceElement> elementElementIterator = sequence.iterator();
            UrlSequence.UrlSequenceElement previous = null;
            UrlSequence.UrlSequenceElement current = null;
            while (elementElementIterator.hasNext()){
                current = elementElementIterator.next();
                if (current.isDynamic()){
                    if (previous != null){
                        String placeholderName = tryGetPlaceholderName(previous, current);
                        current.setMainValue(placeholderName);
                    }
                    urlSequenceContainsDynamicElement = true;
                }
                previous = current;
            }
            if (urlSequenceContainsDynamicElement){
                dynamicSequences.add(sequence);
            }
        }
        return dynamicSequences;
    }


    private String tryGetPlaceholderName(UrlSequence.UrlSequenceElement parentElement, UrlSequence.UrlSequenceElement currentElement) {
        String parentMainValue = parentElement.getMainValue();
        String placeholderName = parentMainValue.endsWith("s") ? parentMainValue.substring(0, parentMainValue.length() - 1) : parentMainValue;
        String placeholderSuffix;
        if (currentElement.getPossibleValues().stream().allMatch(StringAnalysisUtil::isParsableInteger)) {
            placeholderSuffix = "_id";
        } else if (currentElement.getPossibleValues().stream().allMatch(StringAnalysisUtil::isSimilarToName)) {
            placeholderSuffix = "_name";
        } else {
            placeholderSuffix = "_query";
        }
        return String.format("*%s%s*", placeholderName, placeholderSuffix);
    }


    private List<UrlSequence> buildUrlSequencesFromRawUrls(){
        List<UrlSequence> urlSequences = new ArrayList<>();
        Map<String, UrlSequence> headIndex = new HashMap<>();
        for (String url : rawUrls) {
            UrlSequence currentSequence = UrlSequence.from(url);
            UrlSequence sameHeadSequence = headIndex.get(currentSequence.getHead().orElse(null));
            if (sameHeadSequence != null) {
                mergeSequences(currentSequence, sameHeadSequence);
            } else {
                urlSequences.add(currentSequence);
                headIndex.put(currentSequence.getHead().orElse(null), currentSequence);
            }
        }
        return urlSequences;
    }


    private void mergeSequences(UrlSequence sourceSequence, UrlSequence targetSequence) {
        Iterator<UrlSequence.UrlSequenceElement> sourceSequenceIterator = sourceSequence.iterator();
        sourceSequenceIterator.next();
        int position = 1;
        while (sourceSequenceIterator.hasNext()) {
            targetSequence.addObservedElement(position++, sourceSequenceIterator.next().getMainValue());
        }
    }

    @Override
    public String toString() {
        return "DynamicUrlFetcher{" +
                "rawUrls=" + rawUrls +
                '}';
    }
}
