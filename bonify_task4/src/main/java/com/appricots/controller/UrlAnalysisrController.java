package com.appricots.controller;

import com.appricots.service.UrlAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UrlAnalysisrController {


    @Autowired
    UrlAnalysisService urlAnalysisService;


    @RequestMapping(value = "/urls/analyze/detect_dynamic", method = RequestMethod.GET)
    public List<String> analyzeUrls(@RequestParam(name = "urls") List<String> urls){
        return urlAnalysisService.detectDynamicUrlPatterns(urls);
    }

}
