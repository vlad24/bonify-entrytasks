package com.appricots.service;

import com.appricots.domain.DynamicUrlFetcher;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public interface UrlAnalysisService {
    List<String> detectDynamicUrlPatterns(List<String> urls);
}
