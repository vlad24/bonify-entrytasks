package com.appricots.service;

import com.appricots.domain.DynamicUrlFetcher;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class UrlAnalysisServiceImpl implements UrlAnalysisService{
    public List<String> detectDynamicUrlPatterns(List<String> urls) {
        if (urls == null || urls.isEmpty()){
            return Collections.emptyList();
        } else {
            DynamicUrlFetcher urlAnalyzer = new DynamicUrlFetcher();
            urls.forEach(urlAnalyzer::addUrl);
            return urlAnalyzer.detectDynamicPatterns();
        }
    }
}
