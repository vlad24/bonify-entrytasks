package com.appricots.service;

import com.appricots.domain.DynamicUrlFetcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DynamicUrlFetcherTest {

    private DynamicUrlFetcher dynamicPatternFetcher;

    @Before
    public void setUp(){
        dynamicPatternFetcher = new DynamicUrlFetcher();
    }

    @Test
    public void addUrl_WhenSeveralValidUrlsAreAdded_ThenAllOfThemAreRetained() {
        Set<String> urls = new HashSet<>(Arrays.asList(
                "/user/john",
                "/user/maria",
                "/main/about",
                "/main/contact",
                "/user/john/comments/1",
                "/user/john/comments/2"
        ));
        urls.forEach(u -> dynamicPatternFetcher.addUrl(u));
        Assert.assertEquals(dynamicPatternFetcher.getRawUrls(), urls);
    }

    @Test
    public void addUrl_WhenSeveralValidUrlsAreAddedAndThenReset_ThenFetcherHoldsNoUrls() {
        Set<String> urls = new HashSet<>(Arrays.asList(
                "/user/john",
                "/user/maria"
        ));
        urls.forEach(u -> dynamicPatternFetcher.addUrl(u));
        dynamicPatternFetcher.clearRawUrls();
        Assert.assertTrue("Not empty url set after reset", dynamicPatternFetcher.getRawUrls().isEmpty());
    }


    @Test
    public void analyze_WhenSequencesWithDifferentTypesOfDynamicPartsAndQueryStringsGot_ThenAllSequencesAreCorrect() {
        Set<String> urls = new HashSet<>(Arrays.asList(
                "/users/John",
                "/users/Maria",
                "/main/about",
                "/main/contact?type=phone&location=Berlin",
                "/users/John/comments/1?share=true",
                "/users/John/comments/2"
        ));
        urls.forEach(u -> dynamicPatternFetcher.addUrl(u));
        List<String> analysis = dynamicPatternFetcher.detectDynamicPatterns();
        Assert.assertEquals("Wrong size of results", 2, analysis.size());
        Assert.assertTrue("Main variative part was not correctly analyzed", analysis.contains("/main/*main_query*"));
        Assert.assertTrue("Users and comment parts were not correctly analyzed", analysis.contains("/users/*user_name*/comments/*comment_id*"));
    }

    @Test
    public void analyze_WhenSimpleSequenceIsProvidedWithNamesAndGenericParts_ThenResultSequenceIsCorrect() {
        Set<String> urls = new HashSet<>(Arrays.asList(
                "/users/Maria/info/location",
                "/users/Marcelo/info/birthday"
        ));
        urls.forEach(u -> dynamicPatternFetcher.addUrl(u));
        List<String> analysis = dynamicPatternFetcher.detectDynamicPatterns();
        analysis.sort(String::compareTo);
        Assert.assertEquals("Wrong size of results", 1, analysis.size());
        Assert.assertTrue("Main variative part was not correctly analyzed", analysis.contains("/users/*user_name*/info/*info_query*"));
    }

    @Test
    public void analyze_WhenN0DynamicSequencesAreProvided_ThenResultSequencesAreEmpty() {
        Set<String> urls = new HashSet<>(Arrays.asList(
                "/users/Maria/info/location",
                "/home"
        ));
        urls.forEach(u -> dynamicPatternFetcher.addUrl(u));
        List<String> analysis = dynamicPatternFetcher.detectDynamicPatterns();
        Assert.assertTrue("Non empty dynamic url set for static urls", analysis.isEmpty());
    }
}