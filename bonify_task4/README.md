# Task 4
You have an input of the URLs: “/users/Maria/info/location”, “/users/Marcelo/info/
birthday”

Please propose a design for an algorithm that given a set URL paths (as in example
above), can identify dynamic parts and resolve the masked URL to the path like this
“/users/*user_name*/info/*info_query*”
Algorithm should be as efficient as possible based on consideration about the CPU,
Memory and processing time.

*user_name* will represent the username values in the path. In our example it is
Maria or Marcelo.
*info_query* will represent the last part of the path. In our example location or
birthday.

The URLs can be dynamic, and can have multiple dynamic parts.

Expected result: java Spring Boot project with implemented algorithm. Step by step
explanation of how to run the project.

## Solution details

### Architecture
The solution is a simple Spring Boot web application which serves GET requests at HTTP port 8080.
Requests are supposed to have single 'urls' comma-separated string list parameter.


### Algorithm 
Some definitions and assumptions first.

**Url parts**
> Strings A, B, C are called **parts** of the url "/A/B/C".

**Common dynamic part**
> P_n is a common dynamic part for some urls <=> there are at least two urls U1 and U2 which match the following expressions
U1 = /P_1/P_2/.../P_(n-1)/x1/\*; U2 = /P_1/P_2/.../P_(n-1)/x2/\* where x1 != x2 and P_i is some valid url part. 


**Common dynamic pattern**
> Pattern Z is a common dynamic pattern for urls W = {U1, U2, ..., Um} in V = {U1, U2, ..., Un}  <=> all   
urls in W have common set of common dynamic parts D and none of V\W have any common dynamic parts being a subset of D

**Strict-prefix-relation**
> Urls U1 and U2 are said to be in a strict-prefix-relation iff U2 = U1/.+

The solution uses simple matching using comparison on the first part of url and then merging two sequences part-by-part to detect common dynamic parts.
Note that according to the definitions above common dynamic patterns are not possible for urls
 in strict-prefix-relation (just because they cannot by definition have common dynamic parts) <br/>


### On complexity
Let R be the amount of input urls. And M be the maximum number of url parts among all R urls.
The complexity of the proposed algorithm is O(RM): each of M url is mapped to some other url sequence which has the same first part and then the urls are merged
in maximum of R steps.

## How to use
Ensure a port 8080 is not occupied by any other app.
Run
```{bash} 
mvn spring-boot:run
```
to run the project.

To assert the correctness of the solution you cold run the tests:
```{bash}
mvn test
```

Then open your favorite browser, put the following to the address bar and hit 'Enter' 

```{url}
http://localhost:8080//urls/analyze/detect_dynamic?urls=/users/Maria/info/location, /users/Marcelo/info/birthday
```

Change 'urls' parameter to experiment with different algorithm inputs, e.g.

```
/users/John, /main/about, /users/John/comments/1?share=true, /main/contact?type=phone%26location=Berlin, /users/Maria, /users/John/comments/2
```
